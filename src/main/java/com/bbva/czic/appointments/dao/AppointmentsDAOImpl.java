package com.bbva.czic.appointments.dao;

import com.bbva.jee.arq.spring.core.host.protocolo.ps9.ErrorMappingHelper;
import com.bbva.jee.arq.spring.core.servicing.gce.BusinessServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.bbva.czic.appointments.dao.model.ugan.*;
import com.bbva.czic.appointments.business.dto.*;
import com.bbva.jee.arq.spring.core.host.protocolo.ps9.aplicacion.CopySalida;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;

@Component(value = "appointments-dao")
public class AppointmentsDAOImpl implements AppointmentsDAO {

    @Autowired
    private TransaccionUgan ugan;

    @Autowired
    private ErrorMappingHelper emh;

    @Override
    public CopySalida createAppointment(DTOIntAppointment appointment) {
        PeticionTransaccionUgan peticion = new PeticionTransaccionUgan();
        peticion.getCuerpo().getPartes().add(cargarFormatos(appointment));
        RespuestaTransaccionUgan respuesta = ugan.invocar(peticion);
        BusinessServiceException bse = emh.toBusinessServiceException(respuesta);
        if (bse != null) {
            throw bse;
        }
        return respuesta.getCuerpo().getParte(CopySalida.class);
    }

    public FormatoUGANFE00 cargarFormatos(DTOIntAppointment appointment) {
        FormatoUGANFE00 formatoUGANFE00 = new FormatoUGANFE00();

        formatoUGANFE00.setIdecte(appointment.getDocumentType().getId() + appointment.getDocumentNumber());
        formatoUGANFE00.setIndorig(appointment.getOriginAppointmentScheduling());
        formatoUGANFE00.setCodprod(appointment.getSubProduct().toString());
        formatoUGANFE00.setNombres(appointment.getNames());
        formatoUGANFE00.setApellid(appointment.getSurname());
        try {
            String fecha = appointment.getCountryBirth().getName();
            formatoUGANFE00.setFecnaci(new SimpleDateFormat("yyyy-MM-dd").parse(fecha));//Se extrae la fecha de nacimiento
        } catch (Exception e) {
        }
        formatoUGANFE00.setCodpais(appointment.getCountryBirth().getId());
        formatoUGANFE00.setEmail(appointment.getEmail());
        formatoUGANFE00.setPaisres(appointment.getCountryResidence().getId());
        formatoUGANFE00.setDptores(appointment.getStateResidence().getId());
        formatoUGANFE00.setCiudres(appointment.getCityResidence().getId());
        formatoUGANFE00.setTelefon(appointment.getPhone());
        formatoUGANFE00.setCelular(appointment.getCellPhone());
        try {
            String fecha = appointment.getStateResidence().getName();
            formatoUGANFE00.setFecagen(new SimpleDateFormat("yyyy-MM-dd").parse(fecha));//Se extrae la fecha de nacimiento
        } catch (Exception e) {
        }
        formatoUGANFE00.setHorario(appointment.getSchedule());
        formatoUGANFE00.setMontsol(new BigDecimal(appointment.getAmountToFinanced().getAmount()));
        formatoUGANFE00.setVrvivie(new BigDecimal(appointment.getPriceOfHousing().getAmount()));
        formatoUGANFE00.setPlazo(Integer.parseInt(appointment.getPriceOfHousing().getCurrency()));
        formatoUGANFE00.setRtaburo(appointment.getReplyScoreBuro().toString());
        formatoUGANFE00.setAsunto(appointment.getSubject());
        return formatoUGANFE00;
    }

}
