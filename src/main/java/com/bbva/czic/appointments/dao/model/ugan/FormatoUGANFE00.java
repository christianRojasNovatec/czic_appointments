package com.bbva.czic.appointments.dao.model.ugan;


import java.math.BigDecimal;
import java.util.Date;

import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.serializable.RooSerializable;
import org.springframework.roo.addon.tostring.RooToString;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.Formato;
import com.bbva.jee.arq.spring.core.host.TipoCampo;

/**
 * Formato de datos <code>UGANFE00</code> de la transacci&oacute;n <code>UGAN</code>
 * 
 * @author Arquitectura Spring BBVA
 */
@Formato(nombre = "UGANFE00")
@RooJavaBean
@RooToString
@RooSerializable
public class FormatoUGANFE00 {

	/**
	 * <p>Campo <code>IDECTE</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 1, nombre = "IDECTE", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 16, longitudMaxima = 16)
	private String idecte;
	
	/**
	 * <p>Campo <code>INDORIG</code>, &iacute;ndice: <code>2</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 2, nombre = "INDORIG", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 2, longitudMaxima = 2)
	private String indorig;
	
	/**
	 * <p>Campo <code>CODPROD</code>, &iacute;ndice: <code>3</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 3, nombre = "CODPROD", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 4, longitudMaxima = 4)
	private String codprod;
	
	/**
	 * <p>Campo <code>NOMBRES</code>, &iacute;ndice: <code>4</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 4, nombre = "NOMBRES", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 20, longitudMaxima = 20)
	private String nombres;
	
	/**
	 * <p>Campo <code>APELLID</code>, &iacute;ndice: <code>5</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 5, nombre = "APELLID", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 40, longitudMaxima = 40)
	private String apellid;
	
	/**
	 * <p>Campo <code>FECNACI</code>, &iacute;ndice: <code>6</code>, tipo: <code>FECHA</code>
	 */
	@Campo(indice = 6, nombre = "FECNACI", tipo = TipoCampo.FECHA, longitudMinima = 10, longitudMaxima = 10, formato = "yyyy-MM-dd")
	private Date fecnaci;
	
	/**
	 * <p>Campo <code>CODPAIS</code>, &iacute;ndice: <code>7</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 7, nombre = "CODPAIS", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 4, longitudMaxima = 4)
	private String codpais;
	
	/**
	 * <p>Campo <code>EMAIL</code>, &iacute;ndice: <code>8</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 8, nombre = "EMAIL", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 30, longitudMaxima = 30)
	private String email;
	
	/**
	 * <p>Campo <code>PAISRES</code>, &iacute;ndice: <code>9</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 9, nombre = "PAISRES", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 4, longitudMaxima = 4)
	private String paisres;
	
	/**
	 * <p>Campo <code>DPTORES</code>, &iacute;ndice: <code>10</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 10, nombre = "DPTORES", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 4, longitudMaxima = 4)
	private String dptores;
	
	/**
	 * <p>Campo <code>CIUDRES</code>, &iacute;ndice: <code>11</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 11, nombre = "CIUDRES", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 4, longitudMaxima = 4)
	private String ciudres;
	
	/**
	 * <p>Campo <code>TELEFON</code>, &iacute;ndice: <code>12</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 12, nombre = "TELEFON", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 10, longitudMaxima = 10)
	private String telefon;
	
	/**
	 * <p>Campo <code>CELULAR</code>, &iacute;ndice: <code>13</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 13, nombre = "CELULAR", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 10, longitudMaxima = 10)
	private String celular;
	
	/**
	 * <p>Campo <code>FECAGEN</code>, &iacute;ndice: <code>14</code>, tipo: <code>FECHA</code>
	 */
	@Campo(indice = 14, nombre = "FECAGEN", tipo = TipoCampo.FECHA, longitudMinima = 10, longitudMaxima = 10, formato = "yyyy-MM-dd")
	private Date fecagen;
	
	/**
	 * <p>Campo <code>HORARIO</code>, &iacute;ndice: <code>15</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 15, nombre = "HORARIO", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 10, longitudMaxima = 10)
	private String horario;
	
	/**
	 * <p>Campo <code>MONTSOL</code>, &iacute;ndice: <code>16</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 16, nombre = "MONTSOL", tipo = TipoCampo.DECIMAL, longitudMinima = 15, longitudMaxima = 15, decimales = 2)
	private BigDecimal montsol;
	
	/**
	 * <p>Campo <code>VRVIVIE</code>, &iacute;ndice: <code>17</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 17, nombre = "VRVIVIE", tipo = TipoCampo.DECIMAL, longitudMinima = 15, longitudMaxima = 15, decimales = 2)
	private BigDecimal vrvivie;
	
	/**
	 * <p>Campo <code>PLAZO</code>, &iacute;ndice: <code>18</code>, tipo: <code>ENTERO</code>
	 */
	@Campo(indice = 18, nombre = "PLAZO", tipo = TipoCampo.ENTERO, longitudMinima = 2, longitudMaxima = 2)
	private Integer plazo;
	
	/**
	 * <p>Campo <code>RTABURO</code>, &iacute;ndice: <code>19</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 19, nombre = "RTABURO", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 4, longitudMaxima = 4)
	private String rtaburo;
	
	/**
	 * <p>Campo <code>ASUNTO</code>, &iacute;ndice: <code>20</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 20, nombre = "ASUNTO", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 140, longitudMaxima = 140)
	private String asunto;
	
}