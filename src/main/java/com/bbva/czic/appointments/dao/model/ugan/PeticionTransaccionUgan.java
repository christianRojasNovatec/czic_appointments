package com.bbva.czic.appointments.dao.model.ugan;

import com.bbva.jee.arq.spring.core.host.Atributo;
import com.bbva.jee.arq.spring.core.host.Cuerpo;
import com.bbva.jee.arq.spring.core.host.CuerpoMultiparte;
import com.bbva.jee.arq.spring.core.host.MensajeMultiparte;
import com.bbva.jee.arq.spring.core.host.Multiformato;
import com.bbva.jee.arq.spring.core.host.Transaccion;

import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.serializable.RooSerializable;
import org.springframework.roo.addon.tostring.RooToString;

/**
 * <p>Transacci&oacute;n <code>UGAN</code></p>
 * <p>Informaci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Tipo:</b> 1</li>
 *    <li><b>Subtipo:</b> 1</li>
 *    <li><b>Versi&oacute;n:</b> 1</li>
 *    <li><b>Clase de petici&oacute;n:</b> PeticionTransaccionUgan</li>
 *    <li><b>Clase de respuesta:</b> RespuestaTransaccionUgan</li>
 * </ul>
 * </p>
 * <p>Configuraci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Nombre configuraci&oacute;n:</b> default_ps9_mx</li>
 * </ul>
 * </p>
 * <p>Copy de la transacci&oacute;n:</p>
 * <code><pre> * FICHERO: COBD.CN.TMP.QGDTCCT.UGAN
 * UGANAGENDAMIENTO VIVIENDA ONLINE       UG        UG1CUGANBVDUGPO UGANFE00            UGY9  NN3000NNNNNN    SSTN A      SNNNSNNN  NN                2017-04-26CICSDC112017-04-2715.38.32CICSDC112017-04-26-19.12.59.247877CICSDC110001-01-010001-01-01
 * 
 * FICHERO: COBD.CN.TMP.QGDTFDF.UGANFE01
 * UGANFE00�E-AGENDAMIENTO VIVIENDA ONLINE�F�20�00354�01�00001�IDECTE �IDENTIFICACION USUAR�A�016�0�R�        �
 * UGANFE00�E-AGENDAMIENTO VIVIENDA ONLINE�F�20�00354�02�00017�INDORIG�ID ORIGEN           �A�002�0�R�        �
 * UGANFE00�E-AGENDAMIENTO VIVIENDA ONLINE�F�20�00354�03�00019�CODPROD�CODIGO DE SUBPRODUCT�A�004�0�O�        �
 * UGANFE00�E-AGENDAMIENTO VIVIENDA ONLINE�F�20�00354�04�00023�NOMBRES�NOMBRES USUARIO     �A�020�0�O�        �
 * UGANFE00�E-AGENDAMIENTO VIVIENDA ONLINE�F�20�00354�05�00043�APELLID�APELLIDOS USUARIO   �A�040�0�O�        �
 * UGANFE00�E-AGENDAMIENTO VIVIENDA ONLINE�F�20�00354�06�00083�FECNACI�FEC NACIMIENTO USUAR�A�010�0�O�        �
 * UGANFE00�E-AGENDAMIENTO VIVIENDA ONLINE�F�20�00354�07�00093�CODPAIS�COD PAIS            �A�004�0�O�        �
 * UGANFE00�E-AGENDAMIENTO VIVIENDA ONLINE�F�20�00354�08�00097�EMAIL  �EMAIL               �A�030�0�O�        �
 * UGANFE00�E-AGENDAMIENTO VIVIENDA ONLINE�F�20�00354�09�00127�PAISRES�COD PAIS RESIDENCIA �A�004�0�R�        �
 * UGANFE00�E-AGENDAMIENTO VIVIENDA ONLINE�F�20�00354�10�00131�DPTORES�COD DPTO RESIDENCIA �A�004�0�R�        �
 * UGANFE00�E-AGENDAMIENTO VIVIENDA ONLINE�F�20�00354�11�00135�CIUDRES�COD CIUDAD RESIDENCI�A�004�0�R�        �
 * UGANFE00�E-AGENDAMIENTO VIVIENDA ONLINE�F�20�00354�12�00139�TELEFON�NUMERO DE TELEFONO  �A�010�0�O�        �
 * UGANFE00�E-AGENDAMIENTO VIVIENDA ONLINE�F�20�00354�13�00149�CELULAR�NUMERO DE CELULAR   �A�010�0�O�        �
 * UGANFE00�E-AGENDAMIENTO VIVIENDA ONLINE�F�20�00354�14�00159�FECAGEN�FECHA DE AGENDAMIENT�A�010�0�R�        �
 * UGANFE00�E-AGENDAMIENTO VIVIENDA ONLINE�F�20�00354�15�00169�HORARIO�HORARIO DE AGENDAMIE�A�010�0�R�        �
 * UGANFE00�E-AGENDAMIENTO VIVIENDA ONLINE�F�20�00354�16�00179�MONTSOL�MONTO SOLICITADO    �N�015�2�O�        �
 * UGANFE00�E-AGENDAMIENTO VIVIENDA ONLINE�F�20�00354�17�00194�VRVIVIE�VALOR VIVIENDA      �N�015�2�O�        �
 * UGANFE00�E-AGENDAMIENTO VIVIENDA ONLINE�F�20�00354�18�00209�PLAZO  �PLAZO DEL CREDITO   �N�002�0�O�        �
 * UGANFE00�E-AGENDAMIENTO VIVIENDA ONLINE�F�20�00354�19�00211�RTABURO�RESPUESTA BURO      �A�004�0�O�        �
 * UGANFE00�E-AGENDAMIENTO VIVIENDA ONLINE�F�20�00354�20�00215�ASUNTO �ASUNTO AGENDAMIENTO �A�140�0�O�        �
 * 
</pre></code>
 * 
 * @see RespuestaTransaccionUgan
 *
 * @author Arquitectura Spring BBVA
 */
@Transaccion(
	nombre = "UGAN",
	tipo = 1, 
	subtipo = 1,	
	version = 1,
	configuracion = "default_ps9_mx",
	respuesta = RespuestaTransaccionUgan.class,
	atributos = {@Atributo(nombre = "altamiraExtendido", valor = "true"), @Atributo(nombre = "tipoCopy", valor = "FIJA")}
)
@Multiformato(formatos = {FormatoUGANFE00.class})
@RooJavaBean
@RooToString
@RooSerializable
public class PeticionTransaccionUgan implements MensajeMultiparte {
	
	/**
	 * <p>Cuerpo del mensaje de petici&oacute;n multiparte</p>
	 */
	@Cuerpo
	private CuerpoMultiparte cuerpo = new CuerpoMultiparte();
	
	/**
	 * <p>Permite obtener el cuerpo del mensaje de petici&oacute;n multiparte</p>
	 */
	@Override
	public CuerpoMultiparte getCuerpo() {
		return cuerpo;
	}
	
}