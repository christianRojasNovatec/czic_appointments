package com.bbva.czic.appointments.dao.model.ugan;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;

/**
 * Invocador de la transacci&oacute;n <code>UGAN</code>
 * 
 * @see PeticionTransaccionUgan
 * @see RespuestaTransaccionUgan
 */
@Component
public class TransaccionUgan implements InvocadorTransaccion<PeticionTransaccionUgan,RespuestaTransaccionUgan> {
	
	@Autowired
	private ServicioTransacciones servicioTransacciones;
	
	@Override
	public RespuestaTransaccionUgan invocar(PeticionTransaccionUgan transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionUgan.class, RespuestaTransaccionUgan.class, transaccion);
	}
	
	@Override
	public RespuestaTransaccionUgan invocarCache(PeticionTransaccionUgan transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionUgan.class, RespuestaTransaccionUgan.class, transaccion);
	}
	
	@Override
	public void vaciarCache() {}	
}