package com.bbva.czic.appointments.dao;

import com.bbva.czic.appointments.business.dto.*;
import com.bbva.jee.arq.spring.core.host.protocolo.ps9.aplicacion.CopySalida;

public interface AppointmentsDAO {

    public CopySalida createAppointment(DTOIntAppointment appointment);

}
