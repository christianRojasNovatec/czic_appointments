package com.bbva.czic.appointments.business.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bbva.jee.arq.spring.core.log.I18nLog;
import com.bbva.jee.arq.spring.core.log.I18nLogFactory;

import com.bbva.jee.arq.spring.core.servicing.utils.BusinessServicesToolKit;

import com.bbva.czic.appointments.business.dto.DTOIntAppointment;
import com.bbva.czic.appointments.business.ISrvIntAppointments;
import com.bbva.czic.appointments.dao.AppointmentsDAO;
import javax.annotation.Resource;

@Service
public class SrvIntAppointments implements ISrvIntAppointments {

    private static I18nLog log = I18nLogFactory.getLogI18n(SrvIntAppointments.class, "META-INF/spring/i18n/log/mensajesLog");

    @Autowired
    BusinessServicesToolKit bussinesToolKit;

    @Resource(name = "appointments-dao")
    private AppointmentsDAO customersDao;

    @Override
    public void createAppointment(DTOIntAppointment infoAppointment) {
        customersDao.createAppointment(infoAppointment);
    }

}
