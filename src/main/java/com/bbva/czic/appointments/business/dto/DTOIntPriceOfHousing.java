package com.bbva.czic.appointments.business.dto;

public class DTOIntPriceOfHousing {

    public final static long serialVersionUID = 1L;
    private Double amount;
    private String currency;

    public DTOIntPriceOfHousing() {
        //default constructor
    }

    public DTOIntPriceOfHousing(Double amount, String currency) {
        this.amount = amount;
        this.currency = currency;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

}
