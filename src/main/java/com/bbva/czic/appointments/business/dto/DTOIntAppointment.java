package com.bbva.czic.appointments.business.dto;

import java.io.Serializable;

public class DTOIntAppointment implements Serializable {

    public final static long serialVersionUID = 1L;
    private Integer id;
    private String originAppointmentScheduling;
    private Integer subProduct;
    private String names;
    private String surname;
    private DTOIntDocumentType documentType;
    private String documentNumber;
    private String email;
    private String phone;
    private String cellPhone;
    private DTOIntCountryBirth countryBirth;
    private DTOIntCountryResidence countryResidence;
    private DTOIntStateResidence stateResidence;
    private DTOIntCityResidence cityResidence;
    private String dateContact;
    private String subject;
    private String schedule;
    private DTOIntAmountToFinanced amountToFinanced;
    private DTOIntPriceOfHousing priceOfHousing;
    private Integer replyScoreBuro;

    public DTOIntAppointment() {
        //default constructor
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getOriginAppointmentScheduling() {
        return originAppointmentScheduling;
    }

    public void setOriginAppointmentScheduling(String originAppointmentScheduling) {
        this.originAppointmentScheduling = originAppointmentScheduling;
    }

    public Integer getSubProduct() {
        return subProduct;
    }

    public void setSubProduct(Integer subProduct) {
        this.subProduct = subProduct;
    }

    public String getNames() {
        return names;
    }

    public void setNames(String names) {
        this.names = names;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public DTOIntDocumentType getDocumentType() {
        return documentType;
    }

    public void setDocumentType(DTOIntDocumentType documentType) {
        this.documentType = documentType;
    }

    public String getDocumentNumber() {
        return documentNumber;
    }

    public void setDocumentNumber(String documentNumber) {
        this.documentNumber = documentNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getCellPhone() {
        return cellPhone;
    }

    public void setCellPhone(String cellPhone) {
        this.cellPhone = cellPhone;
    }

    public DTOIntCountryBirth getCountryBirth() {
        return countryBirth;
    }

    public void setCountryBirth(DTOIntCountryBirth countryBirth) {
        this.countryBirth = countryBirth;
    }

    public DTOIntCountryResidence getCountryResidence() {
        return countryResidence;
    }

    public void setCountryResidence(DTOIntCountryResidence countryResidence) {
        this.countryResidence = countryResidence;
    }

    public DTOIntStateResidence getStateResidence() {
        return stateResidence;
    }

    public void setStateResidence(DTOIntStateResidence stateResidence) {
        this.stateResidence = stateResidence;
    }

    public DTOIntCityResidence getCityResidence() {
        return cityResidence;
    }

    public void setCityResidence(DTOIntCityResidence cityResidence) {
        this.cityResidence = cityResidence;
    }

    public String getStringContact() {
        return dateContact;
    }

    public void setStringContact(String dateContact) {
        this.dateContact = dateContact;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getSchedule() {
        return schedule;
    }

    public void setSchedule(String schedule) {
        this.schedule = schedule;
    }

    public DTOIntAmountToFinanced getAmountToFinanced() {
        return amountToFinanced;
    }

    public void setAmountToFinanced(DTOIntAmountToFinanced amountToFinanced) {
        this.amountToFinanced = amountToFinanced;
    }

    public DTOIntPriceOfHousing getPriceOfHousing() {
        return priceOfHousing;
    }

    public void setPriceOfHousing(DTOIntPriceOfHousing priceOfHousing) {
        this.priceOfHousing = priceOfHousing;
    }

    public Integer getReplyScoreBuro() {
        return replyScoreBuro;
    }

    public void setReplyScoreBuro(Integer replyScoreBuro) {
        this.replyScoreBuro = replyScoreBuro;
    }

}
