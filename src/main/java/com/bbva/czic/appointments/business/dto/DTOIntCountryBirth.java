package com.bbva.czic.appointments.business.dto;

public class DTOIntCountryBirth {

    public final static long serialVersionUID = 1L;
    private String id;
    private String name;

    public DTOIntCountryBirth() {
        //default constructor
    }

    public DTOIntCountryBirth(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
