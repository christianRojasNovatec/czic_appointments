
package com.bbva.czic.appointments.business.dto;




public class DTOIntResidenceInformation {

    public final static long serialVersionUID = 1L;
    private String id;
    private String name;

    public DTOIntResidenceInformation() {
        //default constructor
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
