package com.bbva.czic.appointments.business;

import com.bbva.czic.appointments.business.dto.DTOIntAppointment;

public interface ISrvIntAppointments {

    public void createAppointment(DTOIntAppointment infoAppointment);

}
