package com.bbva.czic.appointments.facade.v00;

import javax.ws.rs.core.Response;

import com.bbva.czic.appointments.facade.v00.dto.Appointment;


public interface ISrvAppointmentsV00 {
 	public Response createAppointment(Appointment infoAppointment);

	
}