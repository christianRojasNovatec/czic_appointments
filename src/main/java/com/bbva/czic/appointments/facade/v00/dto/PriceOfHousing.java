
package com.bbva.czic.appointments.facade.v00.dto;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


import com.wordnik.swagger.annotations.ApiModelProperty;

@XmlRootElement(name = "priceOfHousing", namespace = "urn:com:bbva:czic:appointments:facade:v00:dto")
@XmlType(name = "priceOfHousing", namespace = "urn:com:bbva:czic:appointments:facade:v00:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class PriceOfHousing
    implements Serializable
{

    public final static long serialVersionUID = 1L;
    @ApiModelProperty(value = "Value of housing", required = true)
    private Double amount;
    @ApiModelProperty(value = "monetary unit.", required = true)
    private String currency;

    public PriceOfHousing() {
        //default constructor
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

}
