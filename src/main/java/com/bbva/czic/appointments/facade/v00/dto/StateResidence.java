
package com.bbva.czic.appointments.facade.v00.dto;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


import com.wordnik.swagger.annotations.ApiModelProperty;

@XmlRootElement(name = "stateResidence", namespace = "urn:com:bbva:czic:appointments:facade:v00:dto")
@XmlType(name = "stateResidence", namespace = "urn:com:bbva:czic:appointments:facade:v00:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class StateResidence
    implements Serializable
{

    public final static long serialVersionUID = 1L;
    @ApiModelProperty(value = "Id of state of residence.", required = true)
    private String id;
    @ApiModelProperty(value = "Name of state of residence.", required = true)
    private String name;

    public StateResidence() {
        //default constructor
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
