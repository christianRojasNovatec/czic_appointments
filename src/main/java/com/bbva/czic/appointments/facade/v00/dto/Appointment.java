package com.bbva.czic.appointments.facade.v00.dto;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.wordnik.swagger.annotations.ApiModelProperty;

@XmlRootElement(name = "appointment", namespace = "urn:com:bbva:czic:appointments:facade:v00:dto")
@XmlType(name = "appointment", namespace = "urn:com:bbva:czic:appointments:facade:v00:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class Appointment
        implements Serializable {

    public final static long serialVersionUID = 1L;
    @ApiModelProperty(value = "Unique appointment identifier", required = true)
    private Integer id;
    @ApiModelProperty(value = "Origin of appointment scheduling.", required = true)
    private String originAppointmentScheduling;
    @ApiModelProperty(value = "By-product code offered by the bank.", required = true)
    private Integer subProduct;
    @ApiModelProperty(value = "Description: User names.", required = true)
    private String names;
    @ApiModelProperty(value = "User names.", required = true)
    private String surname;
    @ApiModelProperty(value = "Object where the type identity document is stored.", required = true)
    private DocumentType documentType;
    @ApiModelProperty(value = "Number of identity document.", required = true)
    private String documentNumber;
    @ApiModelProperty(value = "Email user.", required = true)
    private String email;
    @ApiModelProperty(value = "Phone user.", required = true)
    private String phone;
    @ApiModelProperty(value = "CellPhone user.", required = true)
    private String cellPhone;
    @ApiModelProperty(value = "Country of birth of the user.", required = true)
    private CountryBirth countryBirth;
    @ApiModelProperty(value = "Country of residence of the user", required = true)
    private CountryResidence countryResidence;
    @ApiModelProperty(value = "Department of residence.", required = true)
    private StateResidence stateResidence;
    @ApiModelProperty(value = "City of residence.", required = true)
    private CityResidence cityResidence;
    @ApiModelProperty(value = "String of appointment appointment.", required = true)
    private String dateContact;
    @ApiModelProperty(value = "Description subject.", required = true)
    private String subject;
    @ApiModelProperty(value = "Call schedule.", required = true)
    private String schedule;
    @ApiModelProperty(value = "Amount to be financed", required = true)
    private AmountToFinanced amountToFinanced;
    @ApiModelProperty(value = "Value of housing", required = true)
    private PriceOfHousing priceOfHousing;
    @ApiModelProperty(value = "Reply Score Buro.", required = true)
    private Integer replyScoreBuro;

    public Appointment() {
        //default constructor
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getOriginAppointmentScheduling() {
        return originAppointmentScheduling;
    }

    public void setOriginAppointmentScheduling(String originAppointmentScheduling) {
        this.originAppointmentScheduling = originAppointmentScheduling;
    }

    public Integer getSubProduct() {
        return subProduct;
    }

    public void setSubProduct(Integer subProduct) {
        this.subProduct = subProduct;
    }

    public String getNames() {
        return names;
    }

    public void setNames(String names) {
        this.names = names;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public DocumentType getDocumentType() {
        return documentType;
    }

    public void setDocumentType(DocumentType documentType) {
        this.documentType = documentType;
    }

    public String getDocumentNumber() {
        return documentNumber;
    }

    public void setDocumentNumber(String documentNumber) {
        this.documentNumber = documentNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getCellPhone() {
        return cellPhone;
    }

    public void setCellPhone(String cellPhone) {
        this.cellPhone = cellPhone;
    }

    public CountryBirth getCountryBirth() {
        return countryBirth;
    }

    public void setCountryBirth(CountryBirth countryBirth) {
        this.countryBirth = countryBirth;
    }

    public CountryResidence getCountryResidence() {
        return countryResidence;
    }

    public void setCountryResidence(CountryResidence countryResidence) {
        this.countryResidence = countryResidence;
    }

    public StateResidence getStateResidence() {
        return stateResidence;
    }

    public void setStateResidence(StateResidence stateResidence) {
        this.stateResidence = stateResidence;
    }

    public CityResidence getCityResidence() {
        return cityResidence;
    }

    public void setCityResidence(CityResidence cityResidence) {
        this.cityResidence = cityResidence;
    }

    public String getStringContact() {
        return dateContact;
    }

    public void setStringContact(String dateContact) {
        this.dateContact = dateContact;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getSchedule() {
        return schedule;
    }

    public void setSchedule(String schedule) {
        this.schedule = schedule;
    }

    public AmountToFinanced getAmountToFinanced() {
        return amountToFinanced;
    }

    public void setAmountToFinanced(AmountToFinanced amountToFinanced) {
        this.amountToFinanced = amountToFinanced;
    }

    public PriceOfHousing getPriceOfHousing() {
        return priceOfHousing;
    }

    public void setPriceOfHousing(PriceOfHousing priceOfHousing) {
        this.priceOfHousing = priceOfHousing;
    }

    public Integer getReplyScoreBuro() {
        return replyScoreBuro;
    }

    public void setReplyScoreBuro(Integer replyScoreBuro) {
        this.replyScoreBuro = replyScoreBuro;
    }

    @Override
    public String toString() {
        return "Appointment{" + "id=" + id + ", originAppointmentScheduling=" + originAppointmentScheduling + ", subProduct=" + subProduct + ", names=" + names + ", surname=" + surname + ", documentType=" + documentType + ", documentNumber=" + documentNumber + ", email=" + email + ", phone=" + phone + ", cellPhone=" + cellPhone + ", countryBirth=" + countryBirth + ", countryResidence=" + countryResidence + ", stateResidence=" + stateResidence + ", cityResidence=" + cityResidence + ", dateContact=" + dateContact + ", subject=" + subject + ", schedule=" + schedule + ", amountToFinanced=" + amountToFinanced + ", priceOfHousing=" + priceOfHousing + ", replyScoreBuro=" + replyScoreBuro + '}';
    }
}
