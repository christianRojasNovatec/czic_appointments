package com.bbva.czic.appointments.facade.v00.mapper;

import org.springframework.beans.BeanUtils;

import com.bbva.czic.appointments.facade.v00.dto.ResidenceInformation;
import com.bbva.czic.appointments.facade.v00.dto.DocumentType;
import com.bbva.czic.appointments.facade.v00.dto.CountryResidence;
import com.bbva.czic.appointments.facade.v00.dto.CountryBirth;
import com.bbva.czic.appointments.facade.v00.dto.PriceOfHousing;
import com.bbva.czic.appointments.facade.v00.dto.StateResidence;
import com.bbva.czic.appointments.facade.v00.dto.AmountToFinanced;
import com.bbva.czic.appointments.facade.v00.dto.CityResidence;
import com.bbva.czic.appointments.facade.v00.dto.Appointment;
import com.bbva.czic.appointments.business.dto.DTOIntResidenceInformation;
import com.bbva.czic.appointments.business.dto.DTOIntDocumentType;
import com.bbva.czic.appointments.business.dto.DTOIntCountryResidence;
import com.bbva.czic.appointments.business.dto.DTOIntCountryBirth;
import com.bbva.czic.appointments.business.dto.DTOIntPriceOfHousing;
import com.bbva.czic.appointments.business.dto.DTOIntStateResidence;
import com.bbva.czic.appointments.business.dto.DTOIntAmountToFinanced;
import com.bbva.czic.appointments.business.dto.DTOIntCityResidence;
import com.bbva.czic.appointments.business.dto.DTOIntAppointment;

public class Mapper {

    public static ResidenceInformation residenceInformationMap(DTOIntResidenceInformation dtoIntResidenceInformation) {
        ResidenceInformation residenceInformation = new ResidenceInformation();
        BeanUtils.copyProperties(dtoIntResidenceInformation, residenceInformation);
        return residenceInformation;
    }

    public static DTOIntResidenceInformation dtoIntResidenceInformationMap(ResidenceInformation residenceInformation) {
        DTOIntResidenceInformation dtoIntResidenceInformation = new DTOIntResidenceInformation();
        BeanUtils.copyProperties(residenceInformation, dtoIntResidenceInformation);
        return dtoIntResidenceInformation;
    }

    public static DocumentType documentTypeMap(DTOIntDocumentType dtoIntDocumentType) {
        DocumentType documentType = new DocumentType();
        BeanUtils.copyProperties(dtoIntDocumentType, documentType);
        return documentType;
    }

    public static DTOIntDocumentType dtoIntDocumentTypeMap(DocumentType documentType) {
        DTOIntDocumentType dtoIntDocumentType = new DTOIntDocumentType();
        BeanUtils.copyProperties(documentType, dtoIntDocumentType);
        return dtoIntDocumentType;
    }

    public static CountryResidence countryResidenceMap(DTOIntCountryResidence dtoIntCountryResidence) {
        CountryResidence countryResidence = new CountryResidence();
        BeanUtils.copyProperties(dtoIntCountryResidence, countryResidence);
        return countryResidence;
    }

    public static DTOIntCountryResidence dtoIntCountryResidenceMap(CountryResidence countryResidence) {
        DTOIntCountryResidence dtoIntCountryResidence = new DTOIntCountryResidence();
        BeanUtils.copyProperties(countryResidence, dtoIntCountryResidence);
        return dtoIntCountryResidence;
    }

    public static CountryBirth countryBirthMap(DTOIntCountryBirth dtoIntCountryBirth) {
        CountryBirth countryBirth = new CountryBirth();
        BeanUtils.copyProperties(dtoIntCountryBirth, countryBirth);
        return countryBirth;
    }

    public static DTOIntCountryBirth dtoIntCountryBirthMap(CountryBirth countryBirth) {
        DTOIntCountryBirth dtoIntCountryBirth = new DTOIntCountryBirth();
        BeanUtils.copyProperties(countryBirth, dtoIntCountryBirth);
        return dtoIntCountryBirth;
    }

    public static PriceOfHousing priceOfHousingMap(DTOIntPriceOfHousing dtoIntPriceOfHousing) {
        PriceOfHousing priceOfHousing = new PriceOfHousing();
        BeanUtils.copyProperties(dtoIntPriceOfHousing, priceOfHousing);
        return priceOfHousing;
    }

    public static DTOIntPriceOfHousing dtoIntPriceOfHousingMap(PriceOfHousing priceOfHousing) {
        DTOIntPriceOfHousing dtoIntPriceOfHousing = new DTOIntPriceOfHousing();
        BeanUtils.copyProperties(priceOfHousing, dtoIntPriceOfHousing);
        return dtoIntPriceOfHousing;
    }

    public static StateResidence stateResidenceMap(DTOIntStateResidence dtoIntStateResidence) {
        StateResidence stateResidence = new StateResidence();
        BeanUtils.copyProperties(dtoIntStateResidence, stateResidence);
        return stateResidence;
    }

    public static DTOIntStateResidence dtoIntStateResidenceMap(StateResidence stateResidence) {
        DTOIntStateResidence dtoIntStateResidence = new DTOIntStateResidence();
        BeanUtils.copyProperties(stateResidence, dtoIntStateResidence);
        return dtoIntStateResidence;
    }

    public static AmountToFinanced amountToFinancedMap(DTOIntAmountToFinanced dtoIntAmountToFinanced) {
        AmountToFinanced amountToFinanced = new AmountToFinanced();
        BeanUtils.copyProperties(dtoIntAmountToFinanced, amountToFinanced);
        return amountToFinanced;
    }

    public static DTOIntAmountToFinanced dtoIntAmountToFinancedMap(AmountToFinanced amountToFinanced) {
        DTOIntAmountToFinanced dtoIntAmountToFinanced = new DTOIntAmountToFinanced();
        BeanUtils.copyProperties(amountToFinanced, dtoIntAmountToFinanced);
        return dtoIntAmountToFinanced;
    }

    public static CityResidence cityResidenceMap(DTOIntCityResidence dtoIntCityResidence) {
        CityResidence cityResidence = new CityResidence();
        BeanUtils.copyProperties(dtoIntCityResidence, cityResidence);
        return cityResidence;
    }

    public static DTOIntCityResidence dtoIntCityResidenceMap(CityResidence cityResidence) {
        DTOIntCityResidence dtoIntCityResidence = new DTOIntCityResidence();
        BeanUtils.copyProperties(cityResidence, dtoIntCityResidence);
        return dtoIntCityResidence;
    }

    public static Appointment appointmentMap(DTOIntAppointment dtoIntAppointment) {
        Appointment appointment = new Appointment();
        BeanUtils.copyProperties(dtoIntAppointment, appointment);
        return appointment;
    }

    public static DTOIntAppointment dtoIntAppointmentMap(Appointment appointment) {
        DTOIntAppointment dtoIntAppointment = new DTOIntAppointment();
        //Cantidad a financiar
        AmountToFinanced amountToFinanced = appointment.getAmountToFinanced();
        dtoIntAppointment.setAmountToFinanced(new DTOIntAmountToFinanced(amountToFinanced.getAmount(), amountToFinanced.getCurrency()));
        dtoIntAppointment.setCellPhone(appointment.getCellPhone());
        //Residencia
        CityResidence cityResidence = appointment.getCityResidence();
        dtoIntAppointment.setCityResidence(new DTOIntCityResidence(cityResidence.getId(), cityResidence.getName()));
        //Ciudad de nacimiento
        CountryBirth countryBirth = appointment.getCountryBirth();
        dtoIntAppointment.setCountryBirth(new DTOIntCountryBirth(countryBirth.getId(), countryBirth.getName()));
        //Pais de recidencia
        CountryResidence countryResidence = appointment.getCountryResidence();
        dtoIntAppointment.setCountryResidence(new DTOIntCountryResidence(countryResidence.getId(), countryResidence.getName()));
        // Numero de documento
        dtoIntAppointment.setDocumentNumber(appointment.getDocumentNumber());
        //Tipo de documento
        DocumentType documentType = appointment.getDocumentType();
        dtoIntAppointment.setDocumentType(new DTOIntDocumentType(documentType.getId(), documentType.getName()));
        //Correo 
        dtoIntAppointment.setEmail(appointment.getEmail());
        //Nombres
        dtoIntAppointment.setNames(appointment.getNames());
        //OriginAppointmentScheduling
        dtoIntAppointment.setOriginAppointmentScheduling(appointment.getOriginAppointmentScheduling());
        //Telefono fijo
        dtoIntAppointment.setPhone(appointment.getPhone());
        //Tipo de documento
        PriceOfHousing priceOfHousing = appointment.getPriceOfHousing();
        dtoIntAppointment.setPriceOfHousing(new DTOIntPriceOfHousing(priceOfHousing.getAmount(), priceOfHousing.getCurrency()));
        //ReplyScoreBuro
        dtoIntAppointment.setReplyScoreBuro(appointment.getReplyScoreBuro());
        //Agendamiento
        dtoIntAppointment.setSchedule(appointment.getSchedule());
        //Estado de recidencia
        StateResidence stateResidence = appointment.getStateResidence();
        dtoIntAppointment.setStateResidence(new DTOIntStateResidence(stateResidence.getId(), stateResidence.getName()));
        //Contacto
        dtoIntAppointment.setStringContact(appointment.getStringContact());
        //Subject
        dtoIntAppointment.setSubject(appointment.getSubject());
        //Sub producto
        dtoIntAppointment.setSubProduct(appointment.getSubProduct());
        //Apellido
        dtoIntAppointment.setSurname(appointment.getSurname());
        return dtoIntAppointment;
    }

}
